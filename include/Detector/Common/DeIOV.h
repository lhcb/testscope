/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/Handle.h"
#include "DD4hep/detail/ConditionsInterna.h"

namespace LHCb::Detector {

  namespace detail {

    /**
     *  Base class for interval of validity dependent data
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    struct DeIOVObject : dd4hep::detail::ConditionObject {
      DeIOVObject( dd4hep::DetElement de, dd4hep::cond::ConditionUpdateContext& ctxt );

      /// The dd4hep detector element reference of this gaudi detector element
      dd4hep::DetElement detector;
      /// The alignment object
      dd4hep::Alignment detectorAlignment;
      /// We cache a reference to the geometry information of this gaudi detector element
      dd4hep::PlacedVolume geometry;
      /// We want to cache here the matrix from world to local (=inverse world2local)
      TGeoHMatrix toLocalMatrix;

      /// Printout method to stdout
      virtual void print( int indent, int flags ) const;
    };
  } // End namespace detail

  template <typename ObjectType>
  struct DeIOVElement : dd4hep::Handle<ObjectType> {
    using dd4hep::Handle<ObjectType>::Handle;

    /// Print the detector element's information to stdout
    void print( int indent, int flg ) const { this->access()->print( indent, flg ); }
    /// Compute key value for caching
    static dd4hep::Condition::itemkey_type key( const std::string& value ) {
      return dd4hep::ConditionKey::itemCode( value );
    }
    /// Compute key value for caching
    static dd4hep::Condition::itemkey_type key( const char* value ) { return dd4hep::ConditionKey::itemCode( value ); }

    /// Detector element Class ID (real one from XML, not the enum!)
    int clsID() const { return this->access()->clsID; }
    /// Accessor to detector structure
    dd4hep::DetElement detector() const { return this->access()->detector; }
    /// Accessor to the geometry structure of this detector element
    dd4hep::PlacedVolume geometry() const { return this->access()->geometry; }
    /// Access to the alignmant object to transformideal coordinates
    dd4hep::Alignment detectorAlignment() const { return this->access()->detectorAlignment; }

    /// Checker whether global point is inside this detectos element.
    bool isInside( const ROOT::Math::XYZPoint& globalPoint ) const;

    /** Access to more sophisticated geometry information                      */
    /// Check if the geometry is connected to a logical volume
    bool hasLVolume() const { return geometry().volume().isValid(); }
    /// Check if the geometry is connected to a supporting parent detector element
    bool hasSupport() const { return detector().parent().isValid(); }

    /// Access to transformation matrices
    const TGeoHMatrix& toLocalMatrix() const { return this->access()->toLocalMatrix; }
    const TGeoHMatrix& toGlobalMatrix() const { return this->access()->detectorAlignment.worldTransformation(); }
    const TGeoHMatrix& toLocalMatrixNominal() const { return this->access()->toLocalMatrixNominal; }

    /// Local -> Global and Global -> Local transformations
    ROOT::Math::XYZPoint toLocal( const ROOT::Math::XYZPoint& global ) const {
      return ROOT::Math::XYZPoint( this->access()->detectorAlignment.worldToLocal( ROOT::Math::XYZVector( global ) ) );
    }
    ROOT::Math::XYZPoint toGlobal( const ROOT::Math::XYZPoint& local ) const {
      return ROOT::Math::XYZPoint( this->access()->detectorAlignment.localToWorld( ROOT::Math::XYZVector( local ) ) );
    }
    ROOT::Math::XYZVector toLocal( const ROOT::Math::XYZVector& globalDirection ) const {
      return this->access()->detectorAlignment.worldToLocal( globalDirection );
    }
    ROOT::Math::XYZVector toGlobal( const ROOT::Math::XYZVector& localDirection ) const {
      return this->access()->detectorAlignment.localToWorld( localDirection );
    }
  };

  using DeIOV = DeIOVElement<detail::DeIOVObject>;

} // End namespace LHCb::Detector

/// helper member using IGeometryInfo::isInside
template <typename ObjectType>
bool LHCb::Detector::DeIOVElement<ObjectType>::isInside( const ROOT::Math::XYZPoint& globalPoint ) const {
  // Go through all nodes to check each of them
  const auto& alignment  = detectorAlignment();
  auto        localPoint = toLocal( globalPoint );
  for ( const auto& node : alignment.nodes() ) {
    Double_t p[3]{localPoint.x(), localPoint.y(), localPoint.z()};
    if ( node.volume()->Contains( p ) ) return true;
  }
  return false;
}
