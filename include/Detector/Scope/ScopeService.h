/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DD4hep/Detector.h"
#include "DDCond/ConditionsSlice.h"

#include "Detector/Common/DetectorDataService.h"

namespace scope {

  class ScopeService : public LHCb::Detector::DetectorDataService {

  private:
    std::string m_detDescLocation = "${SCOPE_ROOT}/compact/";
    std::string m_detDescName = "scope.xml";
    std::string m_globalAlignmentLocation = "${SCOPE_ROOT}/data/Conditions/GlobalAlignment.xml";
    std::string m_conditionsLocation = "file://${SCOPE_ROOT}/data/ConditionsIOV/";
  public:
    ScopeService() : DetectorDataService( dd4hep::Detector::getInstance(), {"/world", "scope"} ){};

    void initialize( bool load_conditions = true, std::optional<std::string> conditions_location=std::nullopt, 
                     bool apply_global_alignments = false);

    const dd4hep::Detector& getDetector() const { return m_description; }

    std::shared_ptr<dd4hep::cond::ConditionsSlice> get_slice( size_t iov ) {
      return LHCb::Detector::DetectorDataService::get_slice( iov );
    }
  };
} // End namespace scope
