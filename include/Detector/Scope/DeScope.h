/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>

#include "DD4hep/DetElement.h"

#include "Detector/Common/DeIOV.h"

namespace LHCb::Detector {

  namespace detail {

    struct DeScopeSensorObject : public DeIOVObject {

      DeScopeSensorObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt )
          : DeIOVObject( de, ctxt ) {}
    };

    struct DeScopeSideObject : public DeIOVObject {

      static const int                              sensor_count = 10;
      std::array<DeScopeSensorObject, sensor_count> m_sensors;

      DeScopeSideObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt )
          : DeIOVObject( de, ctxt )
          , m_sensors{{{de.child( "sensor0" ), ctxt},
                       {de.child( "sensor1" ), ctxt},
                       {de.child( "sensor2" ), ctxt},
                       {de.child( "sensor3" ), ctxt},
                       {de.child( "sensor4" ), ctxt},
                       {de.child( "sensor5" ), ctxt},
                       {de.child( "sensor6" ), ctxt},
                       {de.child( "sensor7" ), ctxt},
                       {de.child( "sensor8" ), ctxt},
                       {de.child( "sensor9" ), ctxt}}} {}
    };

    struct DeScopeObject : public DeIOVObject {
      /// Left/right side
      DeScopeSideObject side1, side2;

      DeScopeObject( dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt )
          : DeIOVObject( de, ctxt ), side1( de.child( "side1" ), ctxt ), side2( de.child( "side2" ), ctxt ) {}

      /// Printout method to stdout
      void print( int indent, int flags ) const override;
    };
  } // End namespace detail

  using DeScopeSensor = DeIOVElement<detail::DeScopeSensorObject>;
  using DeScopeSide   = DeIOVElement<detail::DeScopeSideObject>;

  template <typename ObjectType>
  struct DeScopeElement : public DeIOVElement<ObjectType> {

    using DeIOVElement<ObjectType>::DeIOVElement;

    DeScopeSide   getSide1() { return DeScopeSide( &( this->access()->side1 ) ); }
    DeScopeSide   getSide2() { return DeScopeSide( &( this->access()->side2 ) ); }
    DeScopeSensor getSensor( int sensor_number ) {

      const int   sidenb = sensor_number / LHCb::Detector::detail::DeScopeSideObject::sensor_count;
      const auto* side   = ( sidenb == 0 ) ? &( this->access()->side1 ) : &( this->access()->side2 );
      return DeScopeSensor(
          &( side->m_sensors[sensor_number % LHCb::Detector::detail::DeScopeSideObject::sensor_count] ) );
    }
  };

  using DeScope = DeScopeElement<detail::DeScopeObject>;

} // End namespace LHCb::Detector
