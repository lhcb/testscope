//==========================================================================
//  AIDA Detector description implementation
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author  Markus Frank
//  \date    2016-02-02
//  \version 1.0
//
//==========================================================================
#pragma once

// Framework include files
#include "Conditions/ConditionConverter.h"
#include "Conditions/ConditionsReader.h"
#include "DD4hep/ConditionsListener.h"
#include "DD4hep/Printout.h"
#include "DDCond/ConditionsDataLoader.h"
#include "XML/XML.h"

#include <map>
#include <memory>
#include <string>

/// Namespace for the Gaudi framework
namespace LHCb::Detector {

  ///
  /**
   *  \author   M.Frank
   *  \version  1.0
   *  \ingroup  DD4HEP_GAUDI
   */
  class ConditionsLoader : public dd4hep::cond::ConditionsDataLoader {
    using Key    = std::pair<std::string, std::string>;
    using KeyMap = std::map<dd4hep::Condition::key_type, Key>;
    std::unique_ptr<ConditionsReader>  m_reader;
    std::string                        m_readerType, m_directory, m_match, m_dbtag;
    std::map<int, ConditionConverter*> m_converters;
    std::string                        m_iovTypeName;
    const dd4hep::IOVType*             m_iovType = 0;
    /// Load single conditions document
    void loadDocument( dd4hep::xml::UriContextReader& rdr, const std::string& sys_id );

  public:
    /// Default constructor
    ConditionsLoader( dd4hep::Detector& description, dd4hep::cond::ConditionsManager mgr, const std::string& nam );
    /// Default destructor
    virtual ~ConditionsLoader();
    /// Access a conditions loader for a particular class ID
    ConditionConverter& conditionConverter( int class_id );
    /// Initialize loader according to user information
    virtual void initialize() override;
    /// Load  a condition set given a Detector Element and the conditions name according to their validity
    virtual size_t load_single( key_type key, const dd4hep::IOV& req_validity, dd4hep::RangeConditions& conditions );
    /// Load  a condition set given a Detector Element and the conditions name according to their validity
    virtual size_t load_range( key_type key, const dd4hep::IOV& req_validity, dd4hep::RangeConditions& conditions );
    /// Optimized update using conditions slice data
    virtual size_t load_many( const dd4hep::IOV& req_validity, RequiredItems& work, LoadedItems& loaded,
                              dd4hep::IOV& conditions_validity ) override;
  };
} // namespace LHCb::Detector
