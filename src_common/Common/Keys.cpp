/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Framework include files
#include "Detector/Common/Keys.h"
#include "DD4hep/Alignments.h"
#include "DD4hep/Printout.h"

namespace LHCb::Detector {
  /// Static key name: "DetElement-Info-IOV"
  const std::string Keys::deKeyName( "DetElement-Info-IOV" );
  /// Static key: 32 bit hash of "DetElement-Info-IOV". Must be unique for one DetElement
  const dd4hep::Condition::itemkey_type Keys::deKey = dd4hep::ConditionKey::itemCode( Keys::deKeyName );
  /// Key name  of a delta condition "alignment_delta".
  const std::string Keys::deltaName = dd4hep::align::Keys::deltaName;
  /// Key value of a delta condition "alignment_delta".
  const dd4hep::Condition::itemkey_type Keys::deltaKey = dd4hep::align::Keys::deltaKey;

  /// Static key name: "alignment"
  const std::string Keys::alignmentKeyName = dd4hep::align::Keys::alignmentName;
  /// Static key: 32 bit hash of "alignment". Must be unique for one DetElement
  const dd4hep::Condition::itemkey_type Keys::alignmentKey = dd4hep::align::Keys::alignmentKey;

  /// Static key name: "Alignments-Computed"
  const std::string Keys::alignmentsComputedKeyName( "Alignments-Computed" );
  /// Static key: 32 bit hash of "Alignments-Computed". Must be unique for the world
  const dd4hep::Condition::key_type Keys::alignmentsComputedKey =
      dd4hep::ConditionKey::KeyMaker( 0, Keys::alignmentsComputedKeyName ).hash;
} // namespace LHCb::Detector
