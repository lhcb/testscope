#pragma once

#include <DD4hep/Conditions.h>
#include <yaml-cpp/yaml.h>

namespace LHCb::YAMLConverters {
  dd4hep::Condition make_condition( const YAML::detail::iterator_value& cond_data );
}
