//==========================================================================
//  AIDA Detector description implementation
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author  Markus Frank
//  \date    2016-02-02
//  \version 1.0
//
//==========================================================================

// Framework include files
#include "Conditions/ConditionsLoader.h"
#include "Conditions/ConditionsReaderContext.h"
#include "Conditions/ConditionsRepository.h"

// Other dd4hep includes
#include "Conditions/UpgradeTags.h"
#include "DD4hep/Operators.h"
#include "DD4hep/PluginCreators.h"
#include "DD4hep/Printout.h"
#include "DD4hep/detail/ConditionsInterna.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsTags.h"
#include "XML/XML.h"

#include "TTimeStamp.h"

#include "yaml_converters.h"
#include <exception>
#include <set>
#include <sstream>
#include <string_view>
#include <utility>
#include <vector>
#include <yaml-cpp/yaml.h>

/// Standard constructor, initializes variables
LHCb::Detector::ConditionsLoader::ConditionsLoader( dd4hep::Detector& description, dd4hep::cond::ConditionsManager mgr,
                                                    const std::string& nam )
    : ConditionsDataLoader( description, mgr, nam ) {
  declareProperty( "ReaderType", m_readerType );
  declareProperty( "Directory", m_directory );
  declareProperty( "DBTag", m_dbtag );
  declareProperty( "Match", m_match );
  declareProperty( "IOVType", m_iovTypeName );
}

/// Default Destructor
LHCb::Detector::ConditionsLoader::~ConditionsLoader() {}

/// Access a conditions loader for a particular class ID
LHCb::Detector::ConditionConverter& LHCb::Detector::ConditionsLoader::conditionConverter( int class_id ) {
  auto icnv = m_converters.find( class_id );
  if ( icnv == m_converters.end() ) {
    std::stringstream str;
    str << "LHCb_ConditionsConverter_" << class_id;
    ConditionConverter* cnv = dd4hep::createPlugin<ConditionConverter>( str.str(), m_detector, 0, 0 );
    if ( cnv ) {
      m_converters.insert( std::make_pair( class_id, cnv ) );
      return *cnv;
    }
    dd4hep::except( "ConditionsLoader", "Failed to create ConditionConverter for class ID:%d", class_id );
  }
  return *( ( *icnv ).second );
}

/// Initialize loader according to user information
void LHCb::Detector::ConditionsLoader::initialize() {
  std::string typ    = m_readerType;
  const void* argv[] = {"ConditionsReader", this, 0};
  m_reader.reset( dd4hep::createPlugin<ConditionsReader>( typ, m_detector, 2, argv ) );
  ( *m_reader )["Directory"] = m_directory;
  ( *m_reader )["Match"]     = m_match;
  ( *m_reader )["DBTag"]     = m_dbtag;
  m_iovType                  = manager().iovType( m_iovTypeName );
  if ( !m_iovType ) {
    // XXX Error
  }
}

/// Load single conditions document
void LHCb::Detector::ConditionsLoader::loadDocument( dd4hep::xml::UriContextReader& /* rdr */,
                                                     const std::string& /* sys_id */ )
// RequiredItems&         work,
// LoadedItems&           loaded,
// IOV&                   conditions_validity)
{}

/// Load  a condition set given a Detector Element and the conditions name according to their validity
size_t LHCb::Detector::ConditionsLoader::load_range( key_type /* key */, const dd4hep::IOV& /* req_iov */,
                                                     dd4hep::RangeConditions& /* conditions */ ) {
  return 0;
}

/// Access single conditions from the persistent medium
size_t LHCb::Detector::ConditionsLoader::load_single( key_type /* key */, const dd4hep::IOV& /* req_iov */,
                                                      dd4hep::RangeConditions& /* conditions */ ) {
  return 0;
}

/// Optimized update using conditions slice data
size_t LHCb::Detector::ConditionsLoader::load_many( const dd4hep::IOV& req_iov, RequiredItems& work,
                                                    LoadedItems& loaded, dd4hep::IOV& /* conditions_validity */ ) {
  ConditionsReaderContext local;
  size_t                  len = loaded.size();

  local.event_time  = req_iov.keyData.first;
  local.valid_since = 0;
  local.valid_until = 0;

  dd4hep::xml::UriContextReader local_reader( m_reader.get(), &local );

  // First collect all required URIs which need loading.
  // Since one file contains many conditions, we have
  // to create a unique set
  std::string buffer;
  size_t      loaded_len    = loaded.size();
  bool        print_results = isActivePrintLevel( dd4hep::DEBUG );

  std::set<std::string>                                                        urls;
  std::map<dd4hep::Condition::key_type, std::pair<bool, ConditionIdentifier*>> todo;
  loaded.clear();
  for ( const auto& i : work )
    todo.insert( std::make_pair( i.first, std::make_pair( false, (ConditionIdentifier*)i.second->ptr() ) ) );
  try {
    for ( auto i = todo.begin(); i != todo.end(); ++i ) { // O[N]
      bool done = i->second.first;
      if ( done ) { continue; }
      ConditionIdentifier* info = i->second.second;
      if ( !info ) {
        dd4hep::printout( dd4hep::INFO, "ConditionsLoader", "++ CANNOT update condition: [%16llX] [No load info]",
                          i->first );
        continue;
      }
      auto file_info = urls.find( info->sys_id );
      if ( file_info != urls.end() ) { continue; }
      urls.insert( info->sys_id );
      TTimeStamp start;
      // Load from the git cond db here
      bool result = m_reader->load( info->sys_id, &local, buffer );
      if ( result ) {
        auto ext = std::string_view{info->sys_id};
        ext.remove_prefix( ext.find_last_of( '.' ) );
        if ( ext == ".xml" ) {
          dd4hep::ConditionKey::KeyMaker kmaker( info->sys_id_hash, 0 );
          const auto*                    repo = info->repository;
          xml_doc_holder_t               doc(
              xml_handler_t().parse( buffer.c_str(), buffer.length(), info->sys_id.c_str(), &local_reader ) );
          xml_h                          e = doc.root();
          std::vector<dd4hep::Condition> entity_conditons;
          entity_conditons.reserve( 1000 );
          // We implicitly assume that one file may only contain
          for ( xml_coll_t cond( e, _UC( condition ) ); cond; ++cond ) {
            xml_h       x_c        = cond;
            int         cls        = x_c.attr<int>( _LBU( classID ) );
            std::string nam        = x_c.attr<std::string>( _U( name ) );
            kmaker.values.det_key  = info->sys_id_hash;
            kmaker.values.item_key = dd4hep::detail::hash32( nam );
            auto cond_ident        = repo->locations.find( kmaker.hash ); // O[log(N)]
            if ( cond_ident == repo->locations.end() ) {
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Got stray condition %s from %s", nam.c_str(),
                                info->sys_id.c_str() );
              continue;
            }
            dd4hep::Condition::key_type cond_key = cond_ident->second->hash;
            dd4hep::Condition           c        = conditionConverter( cls )( m_detector, cond_ident->second, x_c );
            if ( !c.isValid() ) {
              // Will eventually have to raise an exception
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Failed to load condition %s from %s",
                                nam.c_str(), info->sys_id.c_str() );
              continue;
            }
            c->hash          = cond_ident->second->hash;
            loaded[cond_key] = c;
            entity_conditons.push_back( c );
            if ( cond_key == i->first ) {
              i->second.first = true;
              continue;
            }
            auto cond_itr = todo.find( cond_key ); // O[log(N)]
            if ( cond_itr != todo.end() ) {
              cond_itr->second.first = true;
              continue;
            }
            dd4hep::printout( dd4hep::WARNING, "ConditionsLoader", "++ Got stray but valid condition %s from %s",
                              nam.c_str(), info->sys_id.c_str() );
          }
          dd4hep::IOV::Key              iov_key( local.valid_since, local.valid_until );
          dd4hep::cond::ConditionsPool* pool = manager().registerIOV( *m_iovType, iov_key );
          size_t                        ret  = manager().blockRegister( *pool, entity_conditons );
          if ( ret != entity_conditons.size() ) {
            // Error!
          }
        } else if ( ext == ".yml" || ext == ".yaml" ) {
          dd4hep::ConditionKey::KeyMaker kmaker( info->sys_id_hash, 0 );
          const auto*                    repo = info->repository;
          auto                           data = YAML::Load( buffer );
          std::vector<dd4hep::Condition> entity_conditons;
          entity_conditons.reserve( data.size() );

          for ( const auto& cond_data : data ) {
            std::string nam        = cond_data.first.as<std::string>();
            kmaker.values.det_key  = info->sys_id_hash;
            kmaker.values.item_key = dd4hep::detail::hash32( nam );
            auto cond_ident        = repo->locations.find( kmaker.hash ); // O[log(N)]
            if ( cond_ident == repo->locations.end() ) {
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Got stray condition %s from %s", nam.c_str(),
                                info->sys_id.c_str() );
              continue;
            }
            dd4hep::Condition::key_type cond_key = cond_ident->second->hash;
            dd4hep::Condition           c        = LHCb::YAMLConverters::make_condition( cond_data );
            if ( !c.isValid() ) {
              // Will eventually have to raise an exception
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Failed to load condition %s from %s",
                                nam.c_str(), info->sys_id.c_str() );
              continue;
            }
            c->hash          = cond_ident->second->hash;
            loaded[cond_key] = c;
            entity_conditons.push_back( c );
            if ( cond_key == i->first ) {
              i->second.first = true;
              continue;
            }
            auto cond_itr = todo.find( cond_key ); // O[log(N)]
            if ( cond_itr != todo.end() ) {
              cond_itr->second.first = true;
              continue;
            }
            dd4hep::printout( dd4hep::WARNING, "ConditionsLoader", "++ Got stray but valid condition %s from %s",
                              nam.c_str(), info->sys_id.c_str() );
          }
          dd4hep::IOV::Key              iov_key( local.valid_since, local.valid_until );
          dd4hep::cond::ConditionsPool* pool = manager().registerIOV( *m_iovType, iov_key );
          size_t                        ret  = manager().blockRegister( *pool, entity_conditons );
          if ( ret != entity_conditons.size() ) {
            // Error!
          }
        }
        TTimeStamp stop;
        dd4hep::printout( dd4hep::INFO, "ConditionsLoader", "++ Loaded %4ld conditions from %-48s [%7.3f sec]",
                          loaded.size() - loaded_len, info->sys_id.c_str(), stop.AsDouble() - start.AsDouble() );
        loaded_len = loaded.size();
        if ( !print_results ) continue;
      }
      if ( print_results ) {
        for ( const auto& e : loaded ) {
          const dd4hep::Condition& cond = e.second;
          dd4hep::printout( dd4hep::INFO, "ConditionsLoader", "++ %16llX: %s -> %s", cond.key(), cond->value.c_str(),
                            cond.name() );
        }
      }
    }
  } catch ( const std::exception& e ) {
    dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "+++ Load exception: %s", e.what() );
    throw;
  } catch ( ... ) {
    dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "+++ UNKNWON Load exception." );
    throw;
  }
  return loaded.size() - len;
}

#include "DD4hep/Factories.h"

//==========================================================================
/// Plugin function
static void* create_loader( dd4hep::Detector& description, int argc, char** argv ) {
  const char*                            name = argc > 0 ? argv[0] : "ConditionsLoader";
  dd4hep::cond::ConditionsManagerObject* m    = (dd4hep::cond::ConditionsManagerObject*)( argc > 0 ? argv[1] : 0 );
  return new LHCb::Detector::ConditionsLoader( description, m, name );
}
DECLARE_DD4HEP_CONSTRUCTOR( LHCb_ConditionsLoader, create_loader )
