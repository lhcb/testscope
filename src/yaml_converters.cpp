#include "yaml_converters.h"

#include <DD4hep/AlignmentData.h>
#include <DD4hep/Objects.h>
#include <DD4hep/Printout.h>
#include <DD4hep/detail/ConditionsInterna.h>
#include <exception>
#include <functional>
#include <string>
#include <string_view>
#include <unordered_map>

namespace {
  static std::unordered_map<std::string_view, std::function<dd4hep::Condition( const YAML::detail::iterator_value& )>>
      s_converters{{"!alignment", []( const YAML::detail::iterator_value& cond_data ) {
                      using Delta = dd4hep::Delta;
                      dd4hep::Condition c{cond_data.first.as<std::string>(), "alignment"};
                      auto&             delta = c.bind<Delta>();
                      for ( const auto& param : cond_data.second ) {
                        const auto param_name = param.first.as<std::string>();
                        const auto x = param.second[0].as<double>(), y = param.second[1].as<double>(),
                                   z = param.second[2].as<double>();
                        if ( param_name == "position" || param_name == "dPosXYZ" ) {
                          delta.translation = dd4hep::Position{x, y, z} / 10.0;
                          delta.flags |= Delta::HAVE_TRANSLATION;
                        } else if ( param_name == "rotation" || param_name == "dRotXYZ" ) {
                          delta.rotation = dd4hep::RotationZYX( z, y, x );
                          delta.flags |= Delta::HAVE_ROTATION;
                        } else if ( param_name == "pivot" || param_name == "pivotXYZ" ) {
                          delta.pivot = Delta::Pivot( x, y, z );
                          delta.flags |= Delta::HAVE_PIVOT;
                        } else {
                          dd4hep::printout( dd4hep::ERROR, "Delta", "++ Unknown alignment conditions field: %s",
                                            param_name.c_str() );
                        }
                      }
                      c->setFlag( dd4hep::Condition::ALIGNMENT_DELTA );
                      return c;
                    }}};
  struct _set_default_converter {
    _set_default_converter() { s_converters["?"] = s_converters["!alignment"]; }
  } _;
} // namespace

dd4hep::Condition LHCb::YAMLConverters::make_condition( const YAML::detail::iterator_value& cond_data ) {
  const auto& converter = s_converters.find( cond_data.second.Tag() );
  if ( converter == end( s_converters ) ) throw std::runtime_error( "no converter for " + cond_data.second.Tag() );
  return converter->second( cond_data );
}
