//==========================================================================
//  AIDA Detector description implementation
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================

// Framework includes
#include "Conditions/ConditionConverter.h"
#include "DD4hep/Factories.h"

namespace dd4hep {

  /// Conversion Utilities
  std::string clean_cond_data( char pre, const std::string& data, char post ) {
    std::string d = pre + data + " ";
    size_t      idx, idq;
    for ( idx = d.find_first_not_of( ' ', 1 ); idx != std::string::npos; ) {
      if ( ::isspace( d[idx] ) ) d[idx] = ' ';
      idx = d.find_first_not_of( ' ', ++idx );
    }
    for ( idx = d.find_first_not_of( ' ', 1 ); idx != std::string::npos; ++idx ) {
      if ( d[idx] != ' ' && ::isspace( d[idx] ) ) d[idx] = ' ';
      idq = d.find_first_of( ' ', idx );
      if ( idq != std::string::npos ) {
        idx = d.find_first_not_of( ' ', idq );
        if ( idx == std::string::npos ) break;
        if ( d[idx] != ' ' && ::isspace( d[idx] ) ) d[idx] = ' ';
        d[idq] = ',';
        continue;
      }
      break;
    }
    d[d.length() - 1] = post;
    return d;
  }

  /// Specialized conversion of <param/> and <paramVector> entities in alignments
  template <>
  void Converter<Delta>::operator()( xml_h element ) const {
    std::string         nam  = element.attr<std::string>( _U( name ) );
    std::string         data = clean_cond_data( '(', element.text(), ')' );
    Delta*              a    = _option<Delta>();
    Position            pos;
    const BasicGrammar& g = BasicGrammar::instance<Position>();

    if ( !g.fromString( &pos, data ) ) g.invalidConversion( data, g.type() );
    if ( nam == "dPosXYZ" ) {
      a->translation = pos / 10.0;
      a->flags |= Delta::HAVE_TRANSLATION;
    } else if ( nam == "dRotXYZ" ) {
      a->rotation = RotationZYX( pos.z(), pos.y(), pos.x() );
      a->flags |= Delta::HAVE_ROTATION;
    } else if ( nam == "pivotXYZ" ) {
      a->pivot = ROOT::Math::Translation3D( pos.x(), pos.y(), pos.z() );
      a->flags |= Delta::HAVE_PIVOT;
    } else {
      dd4hep::printout( dd4hep::ERROR, "Delta", "++ Unknown alignment conditions tag: %s", nam.c_str() );
    }
  }

  /// Specialized conversion of <posXYZ/> entities
  template <>
  void Converter<Position>::operator()( xml_h element ) const {
    xml_dim_t dim = element;
    Position* pos = _option<Position>();
    pos->SetXYZ( dim.x( 0.0 ), dim.y( 0.0 ), dim.z( 0.0 ) );
  }
#if 0
  /// Specialized conversion of <posRPhiZ/> entities
  template <> void Converter<PositionRPhiZ>::operator()(xml_h element) const {
    xml_dim_t  dim = element;
    ROOT::Math::Polar2D<double> dim2(dim.r(0.0), dim.phi(0.0));
    Position*  pos = _option<Position>();
    pos->SetXYZ(dim2.X(), dim2.Y(), dim.z(0.0));
  }
  /// Specialized conversion of <rotXYZ/> entities
  template <> void Converter<RotationZYX>::operator()(xml_h element) const {
    xml_dim_t    dim = element;
    RotationZYX* rot = _option<RotationZYX>();
    rot->SetComponents(dim.rotZ(0.0), dim.rotY(0.0), dim.rotX(0.0));
  }
#endif
  namespace {}
} // namespace dd4hep

/// Namespace for the AIDA detector description toolkit
namespace LHCb::Detector {

  /// Alignment conditions
  /**
   *  Form:
   *
   *    <condition classID="6" name="FTSystem">
   *      <paramVector name="dPosXYZ" type="double">0 0 0</paramVector>
   *      <paramVector name="dRotXYZ" type="double">0 0 0</paramVector>
   *    </condition>
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup LHCb_CONDITIONS
   */
  template <>
  dd4hep::Condition ConcreteConditionConverter<6>::operator()( dd4hep::Detector& description, ConditionIdentifier*,
                                                               xml_h             e ) const {
    xml_dim_t                        element = e;
    dd4hep::Condition                cond( element.nameStr(), "alignment" );
    dd4hep::Converter<dd4hep::Delta> conv( description, 0, &cond.bind<dd4hep::Delta>() );
    xml_coll_t( element, _LBU( paramVector ) ).for_each( conv );
    cond->setFlag( dd4hep::Condition::ALIGNMENT_DELTA );
    return cond;
  }

  /// TH2D conditions
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup LHCb_CONDITIONS
   */
  template <>
  dd4hep::Condition ConcreteConditionConverter<1008188>::operator()( dd4hep::Detector& description,
                                                                     ConditionIdentifier*, xml_h e ) const {
    xml_dim_t                        element = e;
    dd4hep::Condition                cond( element.nameStr(), "alignment" );
    dd4hep::Converter<dd4hep::Delta> conv( description, 0, &cond.bind<dd4hep::Delta>() );
    xml_coll_t( element, _LBU( paramVector ) ).for_each( conv );
    cond->setFlag( dd4hep::Condition::ALIGNMENT_DELTA );
    return cond;
  }
} // namespace LHCb::Detector
//==========================================================================
/// Plugin function
template <int TYPE>
static void* create_conditions_converter( dd4hep::Detector&, int, char** ) {
  LHCb::Detector::ConditionConverter* o = new LHCb::Detector::ConcreteConditionConverter<TYPE>();
  return o;
}
// These 2 are the same
DECLARE_DD4HEP_CONSTRUCTOR( LHCb_ConditionsConverter_6, create_conditions_converter<6> )
DECLARE_DD4HEP_CONSTRUCTOR( LHCb_ConditionsConverter_1008106, create_conditions_converter<6> )
