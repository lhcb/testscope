/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/Scope/DeScope.h"
#include "Detector/Common/indent.h"

#include "DD4hep/Printout.h"

void LHCb::Detector::detail::DeScopeObject::print( int indent, int flg ) const {
  std::string prefix = ::getIndentation( indent );
  DeIOVObject::print( indent, flg );
  dd4hep::printout( dd4hep::INFO, "DeScope", "%s+   >> DeScope", prefix.c_str() );
}
