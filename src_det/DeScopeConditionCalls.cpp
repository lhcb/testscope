//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

#include "Detector/Scope/DeScopeConditionCalls.h"
#include "DD4hep/DetectorProcessor.h"
#include "DD4hep/Printout.h"
#include "Math/Transform3D.h"
#include "XML/XML.h"

dd4hep::Condition LHCb::Detector::DeScopeConditionCall::operator()( const dd4hep::ConditionKey&           key,
                                                                    dd4hep::cond::ConditionUpdateContext& ctxt ) {

  auto de = detectorElement( key.hash );
  dd4hep::printout( dd4hep::INFO, "Conditions", "In DeScopeConditionCall::operator() for %s", de.path().c_str() );
  ctxt.condition( Keys::alignmentsComputedKey );
  return DeIOV( new detail::DeScopeObject( de, ctxt ) );
}
