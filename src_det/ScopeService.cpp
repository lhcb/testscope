/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <memory>
#include <mutex>
#include <stdlib.h>

#include "TString.h"
#include "TSystem.h"
#include "TGeoManager.h"

#include "TTimeStamp.h"

#include "Conditions/ConditionsRepository.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/ConditionsPrinter.h"
#include "DD4hep/Printout.h"
#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsIOVPool.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsSlice.h"
#include "Detector/Common/DeAlignmentCall.h"
#include "Detector/Common/DeConditionCall.h"
#include "Detector/Common/Keys.h"

#include "Detector/Scope/ScopeService.h"


namespace scope {

  void ScopeService::initialize( bool load_conditions, std::optional<std::string> scope_root, bool apply_global_alignment) {

    // Setting the scope root if passed
    if (scope_root) {
        dd4hep::printout( dd4hep::INFO, "ScopeService", "Setting scope root to %s ", scope_root.value().c_str());
        setenv("SCOPE_ROOT", scope_root.value().c_str(), 1);
    }
    /// Load the actual description from Detector
    // We use ROOT to expand the variables in the path as it has a convenient
    // function for that.
    auto load_compact = [this](std::string filename) {
          TString cfile(filename);
          gSystem->ExpandPathName( cfile );
          const char* fname = cfile.Data();
          std::cout << "Applying compact: " << fname << std::endl;
          m_description.apply( "DD4hep_CompactLoader", 1, (char**)&fname );
    };    
    load_compact(m_detDescLocation + m_detDescName);
 
    if ( apply_global_alignment ) {
      // Applying the global alignment
      m_description.apply( "DD4hep_GlobalAlignmentInstall", 0, nullptr);

      TString globalAlignment( m_globalAlignmentLocation );
      gSystem->ExpandPathName( globalAlignment );
      const char* alignmentPath = globalAlignment.Data();
      std::cout << "Loading global alignments: " << alignmentPath << std::endl;
      char *args[] = {(char *)alignmentPath, (char *)"BUILD_DEFAULT" };
      m_description.apply( "DD4hep_XMLLoader", 2, args);
    }

    // Now we need to Close the geometry in all cases
    load_compact(m_detDescLocation + "close.xml");

    if ( load_conditions ) {
      std::cout << "Initializing the ConditionsManager " << std::endl;
      TString conditionsLocation( m_conditionsLocation );
      gSystem->ExpandPathName( conditionsLocation );
      dd4hep::printout( dd4hep::INFO, "ScopeService", "Using conditions location: %s", conditionsLocation.Data() );
      DetectorDataService::initialize( conditionsLocation.Data(), "tag1" );
    }
  }
} // namespace scope
