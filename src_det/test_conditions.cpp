/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <iostream>
#include <typeinfo>

#include <TGeoManager.h>
#include <TGeoNavigator.h>

#include "DD4hep/ConditionsPrinter.h"
#include "DD4hep/ConditionsProcessor.h"
#include "DD4hep/Detector.h"
#include "DD4hep/DetectorProcessor.h"
#include "DD4hep/Printout.h"
#include "DDCond/ConditionsPool.h"

#include "Detector/Common/Keys.h"
#include "Detector/Scope/DeScope.h"
#include "Detector/Scope/ScopeService.h"

using namespace std;
using namespace dd4hep;
using namespace dd4hep::cond;

struct Actor : public DetectorProcessor {
  dd4hep::cond::ConditionsPrinter& printer;
  /// Standard constructor
  Actor( ConditionsPrinter& p ) : printer( p ) {}
  /// Default destructor
  virtual ~Actor() {}
  /// Dump method.
  virtual int operator()( DetElement de, int level ) const override {
    const DetElement::Children& children = de.children();
    PlacedVolume                place    = de.placement();
    char                        sens     = place.volume().isSensitive() ? 'S' : ' ';
    char                        fmt[128], tmp[32];
    ::snprintf( tmp, sizeof( tmp ), "%03d/", level + 1 );
    ::snprintf( fmt, sizeof( fmt ), "%03d %%-%ds %%s #Dau:%%d VolID:%%08X %%c", level + 1, 2 * level + 1 );
    printout( INFO, "test_conditions", fmt, "", de.path().c_str(), int( children.size() ), (unsigned long)de.volumeID(),
              sens );
    printer.prefix = string( tmp ) + de.name();
    ( printer )( de, level );
    return 1;
  }
};

double test_slice( const Detector& detector, std::shared_ptr<dd4hep::cond::ConditionsSlice> slice ) {

  // Iterating through the conditions for the scope
  const auto conds = slice->get( detector.detector( "sensor0" ) );
  for ( const dd4hep::Condition& cond : conds ) {
    std::ostringstream os;
    os << "Condition name: " << cond.name();
    printout( INFO, "test_conditions", "%s", os.str().c_str() );
  }

  // Getting the DeScope object itself
  LHCb::Detector::DeScope descope = slice->get( detector.detector( "scope" ), LHCb::Detector::Keys::deKey );
  std::ostringstream      os;
  os << "DeScope position >" << descope.toGlobal( ROOT::Math::XYZPoint( 0, 0, 0 ) );
  double zposition = descope.toGlobal( ROOT::Math::XYZPoint( 0, 0, 0 ) ).z();
  printout( INFO, "test_conditions", "%s", os.str().c_str() );

  for ( int i = 0; i < 20; i++ ) {
    auto               sensor = descope.getSensor( i );
    std::ostringstream os2;
    os2 << "Sensor " << i << " position >" << sensor.toGlobal( ROOT::Math::XYZPoint( 0, 0, 0 ) );
    printout( INFO, "test_conditions", "%s", os2.str().c_str() );
  }

  return zposition;
}

int main( int argc, char* argv[] ) {
  // Initializing the service
  scope::ScopeService svc;
  if ( argc > 1 ) {
    svc.initialize( true, argv[1] );
  } else {
    svc.initialize();
  }
  const Detector& detector   = svc.getDetector();
  auto            slice      = svc.get_slice( 0 );
  double          zposition1 = test_slice( detector, slice );
  auto            slice2     = svc.get_slice( 200 );
  double          zposition2 = test_slice( detector, slice2 );
  auto            slice3     = svc.get_slice( 400 );
  double          zposition3 = test_slice( detector, slice3 );

  if ( zposition1 != 10.0 || zposition2 != 1000.0 || zposition3 != 10.0 ) {
    printout( ERROR, "test_conditions", "Incorrect DeScope position" );
    exit( 1 );
  }
}
