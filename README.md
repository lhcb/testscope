Simple DD4hep detector for test purposes
========================================

Goal
----

The goal of this code is to demonstrate how to develop the geometry and the detector element with DD4hep, in the LHCb way.
For this purpose, some code was imported from the *Detector* project (ex-DDUpgrade, c.f. https://gitlab.cern.ch/lhcb/Detector),
and an example (dummy) detector (called "Scope") was written featuring 2 arrays of silicon sensors enclosed in a beryllium box.

Features
--------
  - Display the detector in its ideal position
  - Apply global misalignment and display the detector. Also allows to trace a particle through the misaligned detector.
  - Loads condition data fro the GitCondDB and apply.

Content
-------

  - compact: XML description of the descriptor and associated files

  - data: Various versions of the conditions data files.
  
  - include/Conditions: Files imported by the Detector project.
  - include/Detector/Common: Files imported by teh Detector project.
  - include/Detector/Scope: Headers for the Scope detector element.

The *src* directory contains DD4hep plugins to act on the Detector instance, e.g.

  - TEST_scope_align.cpp: Test plugin to load conditions in DD4hep format (FOR TEST ONLY).
  - scope_geo.cpp: Plugin to create the Scope geometry
  - scope_cond.cpp: PLugin to create the conditions for the Scope detector.

  The *src_common* directory contains code imported from the Detector project with functionality common to all LHCb sub-detectors.
  The *src_det* directory contains all the code related to the Scope detector.
  

How to build and run
--------------------

see [the documentation](doc/README.md)

