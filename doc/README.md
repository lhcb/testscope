Test Telescope to prototype DD4hep
==================================

Introduction
------------

This project contains a simple detector developed using DD4hep.
Build and installation requires:
  - CMake
  - ROOT
  - Geant4
  - DD4hep (>v1r11)

Content
-------

 *compact/scope.xml* is the main Compact description for the telescope

 Some scripts allow displaying the Geometry and running Geant4 against it.
   - *show_ideal*: Display the ideal geometry
   - *show_misaligned*: Display the  geometry with a global misalignment as defined in *global_alignment.xml*
   

Building and Running
--------------------

To build and test, you need to be on and lxplus7 equivalent machine (CentOS7 with CMVFS, or the Docker image started with `lb-docker-run`).

Make sure you have the LHCb User Environment enabled:
```
source /cvmfs/lhcb.cern.ch/lib/LbEnv
```
(if the environment was already set up, this command does not print anything).

Get the project code and prepare to build
```
git clone --recursive https://gitlab.cern.ch/lhcb/testscope.git
cd testscope
lb-set-platform x86_64-centos7-gcc9-opt
export LCG_VERSION=97a
lb-project-init
```

Then build with
```
make
```

Tests can be run with
```
make test
```

To use the available tools, you can get a shell with the needed environment with
```
./build.$BINARY_TAG/bin/run bash
```
or wrap the call to the commands with the `run` script, e.g.:
```
./build.$BINARY_TAG/bin/run test_scan
```

Example tools
--------------------
  - show_ideal.sh: opens a window that display the ideal placements of the sensors
  - show_misaligned: Uses a global misalignment file (in DD4hep format) to display the detector, misaligned
  - test_scan: Loads the geometry and uses a few lines of ROOT to find the materials encountered on a given path
  - test_conditions: tries loading the condition in LHCb, and loads the Detector element


Tips & tricks
-------------

To dump the position of the volumes:
``` 
geoPluginRun -interpreter -input compact/scope.xml -volmgr -plugin DD4hep_DetectorVolumeDump -posi 
```

To check for overlaps:
``` 
checkOverlaps --compact compact/scope.xml
```
