#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Hackish environment using the LCG views
# We need a version of DD4hep more recent than what is released, hence we override that one


source /cvmfs/lhcb.cern.ch/lib/LbEnv-unstable
lb-set-platform x86_64-centos7-gcc9-opt
export LCG_VERSION=97a
if [ ! -f toolchain.cmake ]; then
  lb-project-init
fi

