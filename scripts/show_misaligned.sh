#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd )"
TOP_DIR=$(dirname ${SCRIPT_DIR})

geoPluginRun -interpreter -input ${TOP_DIR}/compact/scope.xml -volmgr -plugin TEST_scope_align -setup ${TOP_DIR}/data/DD4hepConditions/repository.xml -plugin DD4hep_GlobalAlignmentInstall -plugin DD4hep_XMLLoader ${TOP_DIR}/data/Conditions/GlobalAlignment.xml BUILD_DEFAULT  -plugin DD4hep_DetectorVolumeDump -posi  -plugin DD4hep_GeometryDisplay
