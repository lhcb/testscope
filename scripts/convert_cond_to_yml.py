#!/usr/bin/env python3
def to_dict(xml_file):
    from xml.etree.ElementTree import parse
    tree = parse(xml_file)
    out = {}
    for cond in tree.iter('condition'):
        name = cond.get('name')
        out[name] = {}
        for el in cond.getchildren():
            value = el.text.strip()
            if el.tag.lower().endswith('vector'):
                value = value.split()
                if el.get('type') == 'double':
                    value = [float(v) for v in value]
                elif el.get('type') == 'int':
                    value = [int(v) for v in value]
            else:
                if el.get('type') == 'double':
                    value = float(value)
                elif el.get('type') == 'int':
                    value = int(value)
            out[name][el.get('name')] = value
    return out


def to_yaml(mapping):
    subkey_map = {
        "dPosXYZ": "position",
        "dRotXYZ": "rotation",
        "pivotXYZ": "pivot"
    }
    for key in mapping:
        yield "{}: !alignment".format(key)
        for subkey in mapping[key]:
            yield "  {}: {!r}".format(
                subkey_map.get(subkey, subkey), mapping[key][subkey])


if __name__ == "__main__":
    import sys
    for l in to_yaml(to_dict(sys.argv[1])):
        print(l)
